package com.mgy.example;

import com.mgy.example.common.Student;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Java8Test {
    @Test
    public void testRangeClosed() {
        Stream<int[]> arr = IntStream.rangeClosed(1, 100).boxed().
                flatMap(a -> IntStream.rangeClosed(a, 100).filter(b -> Math.sqrt(a * a + b * b) % 1 == 0).
                        mapToObj(b -> new int[]{a, b, (int) Math.sqrt(a * a + b * b)}));
        arr.forEach(m -> System.out.println(m[0] + "-" + m[1] + "-" + m[2]));
    }



}
