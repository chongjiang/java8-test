package com.mgy.example;

import org.junit.Test;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class FunctionInterfaceTest {

    @Test
    public void predicateTest() {
        Predicate<Integer> predicate = (a) -> a > 5;
        int c = 6;
        predicate = predicate.and(b -> b > 7);
        System.out.println(predicate.test(c));

        predicate = predicate.negate();
        System.out.println(predicate.test(c));
    }

    @Test
    public void consumerTest() {
        Consumer<Integer> consumer = System.out::println;
        consumer = consumer.andThen(System.out::println);
        consumer.accept(2);
    }
}
