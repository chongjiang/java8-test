package com.mgy.example;

import com.mgy.example.common.Student;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OptionalTest {
    @Test
    public void testOptional() {
        List<String> list = null;
        //ofNullable允许参数为空为空
        List<String> list2 = Optional.ofNullable(list).orElse(new ArrayList<>()).stream().collect(Collectors.toList());
        System.out.println("");
    }

    @Test
    public void testOptionalOf() {
        List<String> list = null;
        //of不允许参数为空，否则抛异常
        List<String> list1 = Optional.of(list).orElse(null);
    }

    @Test
    public void testStreamOf() {
        List<Student> list1 = new ArrayList<>();
        List<Student> list2 = new ArrayList<>();
        Student s = new Student();
        s.setId(1);
        list2.add(s);

        Student s2 = new Student();
        s2.setId(2);
        list1.add(s2);

        List<Student> list3 = Stream.of(Optional.ofNullable(list1).orElse(new ArrayList<>()), list2).flatMap(Collection::stream).collect(Collectors.toList());
        Assert.assertEquals(list3.size(), 2);

        List<Integer> ids = Stream.of(Optional.ofNullable(list1).orElse(new ArrayList<>()), list2).flatMap(Collection::stream).map(Student::getId).collect(Collectors.toList());
        Assert.assertEquals(ids.size(), 2);

        List<Student> list4 = Stream.of(Optional.ofNullable(list1).orElse(new ArrayList<>()).stream(), Optional.ofNullable(list2).orElse(new ArrayList<>()).stream()).flatMap(Function.identity()).collect(Collectors.toList());
        Assert.assertEquals(list3.size(), 2);

        //如果参数是数组允许元素为null
        Stream<List<Student>> list5 = Stream.of(null, list1);
        Assert.assertNotNull(list5);

        //不允许参数为null
        Stream<List<Student>> list6 = Stream.of(null);
        Assert.assertNotNull(list6);
    }

    @Test
    public void testToMap() {
        List<Student> list1 = new ArrayList<>();
        Student s = new Student();
        s.setId(1);
        s.setName("张三");
        list1.add(s);

        Student s2 = new Student();
        s2.setId(2);
        s2.setName("李四");
        list1.add(s2);

        Student s3 = new Student();
        s3.setId(2);
        s3.setName("王五");
        list1.add(s3);
        //如果key重复则抛异常
        // Map<Integer, Student> map = list1.stream().collect(Collectors.toMap(Student::getId, Function.identity()));

        Map<Integer, Student> map2 = list1.stream().collect(Collectors.toMap(Student::getId, Function.identity(), (t1, t2) -> t2));
        Assert.assertEquals(map2.size(), 2);

        List<Student> list2 = null;
        Map<Integer, Student> map3 = Optional.ofNullable(list2).orElse(new ArrayList<>()).stream().collect(Collectors.toMap(Student::getId, Function.identity()));
        Student ss = map3.get(2);
        Assert.assertNull(map3);
    }

    @Test
    public void testfilter() {

        Student s = new Student();
        s.setId(1);
        s.setName("张三");

        Optional.of(s).filter(m -> "张三".equals(m.getName())).ifPresent(m -> System.out.println(m.getName()));
    }

    @Test
    public void flatMap(){
        Student s = new Student();
        s.setId(1);
        s.setName("张三");

        Optional.of(s).flatMap(t->Optional.ofNullable(t.getSchool())).ifPresent(m->System.out.println(m.getName()));
    }

    @Test
    public void map(){
        Student s = new Student();
        s.setId(1);
        s.setName("张三");
        Optional.of(s).map(m->m.getSchool()).ifPresent(m->System.out.println(m.getName()));
    }
}
