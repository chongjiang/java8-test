package com.mgy.example;

import com.mgy.example.common.Student;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class CompletableFutureTest {

    @Test
    public void thenCombineTest() throws ExecutionException, InterruptedException {
        Future<Integer> future = CompletableFuture.supplyAsync(() -> 2).thenCombine(CompletableFuture.supplyAsync(() -> 5), (price, rate) -> price * rate);
        System.out.println(future.get());
    }

    @Test
    public void thenCombineAsyncTest() throws ExecutionException, InterruptedException {
        Future<Integer> future = CompletableFuture.supplyAsync(() -> 2).thenCombineAsync(CompletableFuture.supplyAsync(() -> 5), (price, rate) -> price * rate);
        System.out.println(future.get());

    }

    @Test
    public void completableFutureJoinTest() {
        List<Student> students = new ArrayList<>();
        Student s1 = new Student();
        s1.setId(1);
        s1.setName("张三");
        s1.setAge(12);
        students.add(s1);

        Student s2 = new Student();
        s2.setId(2);
        s2.setName("李四");
        s2.setAge(14);
        students.add(s2);

        Student s3 = new Student();
        s3.setId(3);
        s3.setName("王五");
        s3.setAge(16);
        students.add(s3);

        List<CompletableFuture<String>> list = students.stream().map(t -> CompletableFuture.supplyAsync(() -> t.getName() + ":" + t.getAge())).collect(Collectors.toList());
        List<String> result = list.stream().map(CompletableFuture::join).collect(Collectors.toList());
        System.out.println(result.size());
    }

    @Test
    public void test2() {
        List<Student> students = new ArrayList<>();
        Student s1 = new Student();
        s1.setId(1);
        s1.setName("张三");
        s1.setAge(12);
        students.add(s1);

        Student s2 = new Student();
        s2.setId(2);
        s2.setName("李四");
        s2.setAge(14);
        students.add(s2);

        Student s3 = new Student();
        s3.setId(3);
        s3.setName("王五");
        s3.setAge(16);
        students.add(s3);

        Student s4 = new Student();
        s4.setId(4);
        s4.setName("赵六");
        s4.setAge(17);
        students.add(s4);

        System.out.println("线程数量：" + Runtime.getRuntime().availableProcessors());


//
//        ExecutorService executor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(), Runtime.getRuntime().availableProcessors(), 60L, TimeUnit.SECONDS,
//                new LinkedBlockingQueue<>(), new ThreadFactoryBuilder().setNameFormat("test-pool-%d").build());


        Executor executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors(), (r) -> {
            Thread t = new Thread(r, "thread-test");
            t.setDaemon(true);
            return t;
        });


        List<CompletableFuture<String>> list = students.stream().map(t -> CompletableFuture.supplyAsync(() -> {
            String result = t.getName() + ":" + t.getAge();
            if (t.getId() == 3) {
                throw new RuntimeException("抛出一个异常");
            }
            System.out.println(Thread.currentThread().getName());
            return result;
        }, executor).exceptionally(e -> {
            System.out.println(e.getMessage());
            return "null";
        })).collect(Collectors.toList());
        List<String> result = list.stream().map(CompletableFuture::join).collect(Collectors.toList());

        System.out.println("");

    }


    @Test
    public void test3() {
        System.out.print(Math.round(1.5));
    }
}
