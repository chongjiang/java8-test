package com.mgy.example;

import org.junit.Test;


import java.nio.charset.Charset;
import java.util.zip.CRC32;

public class CRC32Test {
    @Test
    public void test1() {
        String content = "感谢亲光临本店，我们将一如既往的为您提供优质.时尚.高性价比的服务，我们完善的售前.售后服务，将全程为您的愉快购物保驾护航，希望您在本店购物愉快！如有任何问题，可随时与本店联系！记得收藏下本店，没事的时候常来逛逛，一定会有意外的惊喜！期待再合作啦!顺祝：工作顺利，生活愉快，身体健康！1";
        CRC32 crc = new CRC32();
        crc.update(content.getBytes(Charset.forName("utf-8")));
        System.out.print(crc.getValue());
        //3959125953   1867835110
    }
}
